const mailgun = require('mailgun-js')({
    apiKey: 'key-bf20a10328c671f87fcf5d4460349246',
    domain: 'meltag.com'
});
const

    mailHelpers = {

        sendEmail: (recipient, message, attachment, deleteFile = false) =>

            new Promise((resolve, reject) => {
                const data = {
                    from: 'Meltag <admin@meltag.com>',
                    to: recipient,
                    subject: message.subject,
                    text: message.text,
                    inline: attachment,
                    html: message.html,
                };

                mailgun.messages().send(data, (error) => {
                    if (error) {
                        return reject(error);
                    }
                    return resolve();
                });
            }).then(() => {

                if (attachment && deleteFile) {
                    unlink(attachment, (err) => {
                        if (err) {
                            console.log(err.message);
                            return
                        }
                    });
                }
            })
    }

module.exports = mailHelpers;
